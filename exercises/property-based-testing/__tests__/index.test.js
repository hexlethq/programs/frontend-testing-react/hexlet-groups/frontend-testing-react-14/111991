const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

describe('sort()', () => {
  test('should contain the same items', () => {
    const count = (tab, element) => tab.filter((v) => v === element).length;
    fc.assert(
      fc.property(fc.array(fc.integer()), (data) => {
        const sorted = sort(data);
        expect(sorted.length).toEqual(data.length);
        data.forEach((item) => {
          expect(count(sorted, item)).toEqual(count(data, item));
        });
      }),
    );
  });

  test('should have the same length with original array', () => {
    fc.assert(
      fc.property(fc.array(fc.integer()), (data) => {
        const sorted = sort(data);
        expect(sorted.length).toEqual(data.length);
      }),
    );
  });

  test('can be called multiple times (chained)', () => {
    fc.assert(
      fc.property(fc.array(fc.integer()), (data) => {
        const sorted = sort(data);

        const sortedTwice = sort(sorted);
        expect(sorted).toEqual(sortedTwice);
      }),
    );
  });

  test('should produce ordered array', () => {
    fc.assert(
      fc.property(fc.array(fc.integer()), (data) => {
        const sorted = sort(data);
        expect(sorted).toBeSorted();
      }),
    );
  });
});
