describe('Object.assign()', () => {
  test('Return a reference of the passed object (first arg)', () => {
    const src = { k: 'v', b: 'b' };
    const result = Object.assign(src);

    expect(result).toBe(src);
  });

  test('Merging objects', () => {
    const src = { k: 'v', b: 'b' };
    const target = { k: 'v2', a: 'a' };
    const result = Object.assign(target, src);

    expect(result).toEqual({ k: 'v', a: 'a', b: 'b' });
    expect(result).toEqual(target);
  });

  test('Merging objects with same properties', () => {
    const src1 = { a: 'a', b: 'b' };
    const src2 = { b: 'b1', c: 'c' };
    const target = { c: 'c1', d: 'd' };
    const result = Object.assign(target, src1, src2);

    expect(result).toEqual({
      c: 'c', d: 'd', a: 'a', b: 'b1',
    });
    expect(result).toEqual(target);
  });

  test('Cloning an object', () => {
    const src = { k: 'v', b: 'b' };
    const target = { k: 'v2', a: 'a' };
    const result = { ...target, ...src };

    expect(result).toEqual({ k: 'v', a: 'a', b: 'b' });
  });

  test('Primitives will be wrapped to objects', () => {
    const src1 = 'abc';
    const src2 = true;
    const src3 = 10;
    const src4 = Symbol('foo');

    const result = {
      ...src1, ...null, ...src2, ...undefined, ...src3, ...src4,
    };

    expect(result).toEqual({ 0: 'a', 1: 'b', 2: 'c' });
  });

  test('Copying symbol-typed properties', () => {
    const src1 = { a: 1 };
    const src2 = { symbol: Symbol.for('a.b.c') };

    const result = { ...src1, ...src2 };

    expect(result).toEqual({ a: 1, symbol: Symbol.for('a.b.c') });
  });

  test('Merging objects with nested properties', () => {
    const src = { a: { b: 'b' } };
    const target = { a: 'a', b: { c: 'c' } };

    const result = Object.assign(target, src);

    expect(result).toEqual({
      a: { b: 'b' },
      b: { c: 'c' },
    });
  });
});
