const faker = require('faker');

// BEGIN
describe('createTransaction()', () => {
  test('should be transaction object', () => {
    const data = faker.helpers.createTransaction();
    expect(data).toMatchObject({
      amount: expect.stringMatching(/^[0-9]+\.[0-9]{1,2}/),
      date: expect.any(Date),
      business: expect.any(String),
      name: expect.any(String),
      type: expect.any(String),
      account: expect.stringMatching(/[0-9]/),
    });
  });

  it('should generate uniq data', () => {
    const data1 = faker.helpers.createTransaction();
    const data2 = faker.helpers.createTransaction();

    expect(data1).not.toStrictEqual(data2);
  });
});
// END
