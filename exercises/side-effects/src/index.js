const fs = require('fs');

const categories = {
  patch: 'patch',
  minor: 'minor',
  major: 'major',
};

// BEGIN
const update = (version, versionCategory = 'patch') => {
  const [major, minor, patch] = version.split('.').map(Number);

  switch (versionCategory) {
    case categories.patch:
      return `${major}.${minor}.${patch + 1}`;
    case categories.minor:
      return `${major}.${minor + 1}.0`;
    case categories.major:
      return `${major + 1}.0.0`;
    default:
      return version;
  }
};

const upVersion = (path, versionCategory) => {
  try {
    const data = fs.readFileSync(path, 'utf8');

    const { version, ...rest } = JSON.parse(data);
    const nextVersion = update(version, versionCategory);
    const newData = JSON.stringify({ ...rest, version: nextVersion });

    fs.writeFileSync(path, newData);
  } catch (err) {
    throw new Error(err);
  }
};
// END

module.exports = { upVersion };
