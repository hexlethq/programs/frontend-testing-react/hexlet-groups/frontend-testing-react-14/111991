const fs = require('fs');
const path = require('path');
const os = require('os');

const { upVersion } = require('../src/index.js');

// BEGIN
const fixtureName = 'package.json';

const getFixturePath = (filename) => path.join('__fixtures__', filename);

const readFile = (filename) => fs.readFileSync(filename, 'utf-8');

const extractData = (filePath) => {
  const data = readFile(filePath);
  return JSON.parse(data);
};

let pathToTempDir;

beforeEach(() => {
  pathToTempDir = fs.mkdtempSync(path.join(os.tmpdir(), 'upVersion'));

  fs.copyFileSync(getFixturePath(fixtureName), path.join(pathToTempDir, fixtureName));
});

describe('upVersion', () => {
  test('should update patch', () => {
    const packageFilePath = path.join(pathToTempDir, fixtureName);

    upVersion(packageFilePath);

    const data = extractData(packageFilePath);

    expect(data).toEqual({ version: '1.3.3' });
  });

  test('should update minor', () => {
    const packageFilePath = path.join(pathToTempDir, fixtureName);

    upVersion(packageFilePath, 'minor');

    const data = extractData(packageFilePath);

    expect(data).toEqual({ version: '1.4.0' });
  });

  test('should update major', () => {
    const packageFilePath = path.join(pathToTempDir, fixtureName);

    upVersion(packageFilePath, 'major');

    const data = extractData(packageFilePath);

    expect(data).toEqual({ version: '2.0.0' });
  });
});

// END
